import os
from .settings import *

#production settings go here
DEBUG=False

ALLOWED_HOSTS=["35.222.77.254","autoxmd.4k-pixellate.me","localhost","0.0.0.0","0,0,0,0:8000"]

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': secrets['db_name'],
		'USER': secrets['db_user'],
		'PASSWORD': secrets['db_password'],
		'HOST': 'localhost',
		'PORT': '',
	}
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

#security check
SECURE_CONTENT_TYPE_NOSNIFF=True
SECURE_BROWSER_XSS_FILTER=True
#SECURE_SSL_REDIRECT=True (handled by nginx)
SESSION_COOKIE_SECURE=True
CSRF_COOKIE_SECURE=True
X_FRAME_OPTIONS='DENY'
