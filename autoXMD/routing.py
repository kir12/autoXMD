from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import converter.routing

application = ProtocolTypeRouter({
	'websocket':AuthMiddlewareStack(
		URLRouter(
			converter.routing.websocket_urlpatterns
		)
	),
})

#channel_routing = [
	#route('websocket.connect', ws_connect),
	#route('websocket.disconnect', ws_disconnect),
#]
