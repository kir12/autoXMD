@12, for backing piano
@23 for a lead or for backing power chords
@29 for punchy bass
@30 for backing chords
@43 for acompaniment arpeggios
@63 for bells
@74 for normal bass
@101 for lead piano
@181 for acompaniment arpeggios together with SSG
@250 for lead and rarely for backing power chords
it's rare for me  to use other than those since zun used mostly these anyways
