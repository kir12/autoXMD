from django.urls import path

from . import views

app_name='converter'

urlpatterns = [
	path('', views.index, name='index'),
	path('initial_read', views.initial_read, name='initial_read'),
	path('instrument_fill',views.instrument_fill,name='instrument_fill')
]
