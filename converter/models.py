import os
from django.db import models

class MusicXML(models.Model):
	musicFile = models.FileField(upload_to="xml/",null=True)
	msczFile = models.FileField(upload_to="xml/",null=True,blank=True)
	mmlFile = models.FileField(upload_to="PMD/songs/",null=True,blank=True)
	m2File = models.FileField(upload_to="PMD/songs/",null=True,blank=True)
	flacFile = models.FileField(upload_to="PMD/songs/",null=True,blank=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	instrument_count = models.IntegerField(null=True,blank=True)

class InstrumentName(models.Model):
	name = models.CharField(max_length=100)
	xmlFile = models.ForeignKey('MusicXML',on_delete=models.CASCADE,related_name='instrumentname')
