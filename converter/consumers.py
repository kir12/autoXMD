from channels.generic.websocket import WebsocketConsumer
from core.mmlmake import gen_all_instruments,mmlmake
from core.msdos import compileM, gen_flac
from .models import MusicXML
from .views import relative_path #helper function
import json
import time
import re

#standard percent updates
def broadcast(ws_self,percent,message):
	ws_self.send(text_data=json.dumps({"percent":percent,"message":message}))

#final update which issues output
def final_broadcast(ws_self,payload,musicXMLObject):

	file_paths = {}
	if musicXMLObject.mmlFile:
		file_paths["<code>.mml</code> File"]=musicXMLObject.mmlFile.url
	if musicXMLObject.m2File:
		file_paths["<code>.m2</code> File"]=musicXMLObject.m2File.url
	if musicXMLObject.flacFile:
		file_paths["<code>.flac</code> File"]=musicXMLObject.flacFile.url
	ws_self.send(text_data=json.dumps({"fontawesome":payload[0],"status":payload[1],"message":payload[2],"console":payload[3],'file_paths':file_paths}))

class ChatConsumer(WebsocketConsumer):
	def connect(self):
		self.accept()
	def disconnect(self, close_code):
		pass

	def receive(self, text_data):
		#refining rawest data
		string_list,pk = text_data.split("_")
		all_instruments = gen_all_instruments(eval(string_list))
		musicXMLObject = MusicXML.objects.get(pk=int(pk))
		broadcast(self,10,"Opening MusicXML file in <code>music21</code>")

		#mml generation
		mml_filename, basefilename = mmlmake(musicXMLObject.musicFile.path,all_instruments,[],[broadcast,self])
		musicXMLObject.mmlFile.name = relative_path(mml_filename)
		musicXMLObject.save()
		broadcast(self,50,"Processing <code>.mml</code> file in Dosemu")

		#pass into dosemu
		output_results = compileM(musicXMLObject.mmlFile.path,basefilename)
		if(output_results[0]==False): #case: PMD error
			response_json = ["bomb","danger","PMD reported an error during compiling, here's what we found.",output_results[2]]
		else:
			#extracts length of each part to ensure that no runtime problems came up
			part_lengths = [int(re.findall(r'\d+',line)[0]) for line in output_results[2].splitlines() if 'Part' in line]
			print(part_lengths)
			musicXMLObject.m2File.name = relative_path(output_results[1])
			musicXMLObject.save()
			if len(set(part_lengths))>1:#case: PMD compiled successfully but something may have gone wrong
				response_json = ["exclamation-triangle","warning","PMD compiled successfully, but we think something might've gone wrong. Take a look.",output_results[2]]
			else:  #case: everything went smoothly
				response_json = ["check","success","PMD compiled successfully, and things are looking sparkly! Take a look.",output_results[2]]

		if response_json[1] != "danger":
			broadcast(self,75,"Recording and Exporting to <code>.flac</code>")
			musicXMLObject.flacFile.name = relative_path(gen_flac(musicXMLObject.m2File.path))
			musicXMLObject.save()

		#final broadcasts back to user
		broadcast(self,100,"Compiling Complete! Please check output.")
		final_broadcast(self,response_json,musicXMLObject)
