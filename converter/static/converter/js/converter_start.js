$('#MusicXML-submission').on('submit',function(event){
	event.preventDefault(); //disable standard sbumission
	$("#errors").html("<div class = 'alert alert-warning text-center' role = 'alert'><i class='fas fa-spinner fa-spin'></i> <b>Girls are preparing</b>, please wait warmly and have some tea.</div>");
	$.ajax({
		url:'initial_read',
		type:'POST',
		data:new FormData($("#MusicXML-submission")[0]), //retrieves file
		success:function(data,b,c){ //success
			$("#errors").html("<div class = 'alert alert-success text-center' role = 'alert'><i class='fas fa-checkmark'></i> <b>We read your score!</b> You will be re-directed momentarily.</div>");
			window.location.replace("/converter/instrument_fill?pk=" + data['pk'] + "&num_instruments="+data['num_instruments']);
		},
		error:function(xhr,status,error){ //failure
			console.log();
			$("#errors").html("<div class='alert alert-danger text-center' role='alert'><i class='fas fa-exclamation-triangle'></i> <b>Error:</b> " + xhr.responseJSON['output'] + "</div>");
		},
		//must always be in place to ensure jquery does no analysis
		cache: false,
		contentType: false,
		processData: false,
	});
});

