//global vars preserved as (mostly) constant
var num_instruments;
var pk;
var inputs;

//declares and sets up websocket connection
var wss_type = location.protocol != "https:" ? "ws" : "wss";
var socket = new WebSocket(wss_type+'://' + window.location.host + '/wsUpdate');
socket.onopen = function open() {
	console.log('WebSockets connection created.');
};

socket.onmessage = function(event){
	data = JSON.parse(event['data']);
	if(data['fontawesome']== undefined){//standard percent update
		$(".progress-bar").animate({'width':data['percent']+'%'},20);
		$("#progress-desc").html("<b>Status</b>: "+ data['message'] + "...");
	}
	else{
		$("#progress-group").slideToggle();
		//generate HTML for download links
		file_paths = ``;
		for(const [key,value] of Object.entries(data['file_paths'])){
			file_paths+=`
				<div class = 'col-md-4 text-center'>
					<a target = "_blank" class="btn btn-info btn-block" href='` + value  +`' role="button"><b>` + key + `</b></a> 
				</div>
			`
		}
		//generate and show results dialog
		$("#errors").html(
			`<div class = 'col-md-10'>
				<div class='alert alert-` + data['status'] + ` mb-0 text-center' role='alert'> 
					<p><i class='fas fa-` + data['fontawesome'] + `'></i> ` + data['message'] + `</p>
					<code><pre>` + data['console'] + `</pre></code>
					<p>Download Links:</p>
					<div class = "row justify-content-center mb-3">` + file_paths + `</div>
					<p class = "mb-0"><b>Download files will not be saved, so please take them now.</b></p>
				</div>
			</div>`
		)
		$("#errors").slideToggle();
	}
}

if (socket.readyState == WebSocket.OPEN) {
	socket.onopen();
}

//changes the headers of instrument blocks when a PC-98 selector is changed
function changeHeader(color, text,origin){
	$(origin).parent().parent().css("border-top","1px solid "+color);
	$(origin).parent().parent().children('.instrument-class').css("background-color",color);
	$(origin).parent().parent().children('.instrument-class').children('small').text(text);
}

//credits: https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
}

//retriieves num instruments and pk as global vars for quick usage
$(document).ready(function(){
	num_instruments=getUrlParameter('num_instruments');
	pk=getUrlParameter('pk');
});

$('.form-control').on('change',function(){
	//determines what setting to change header to
	if(this.value.indexOf('SSG')==0){
		changeHeader("#3060d0ff","SSG",this);
	}
	else if (this.value == "Choose"){
		changeHeader("#0a1128","TBD",this);
	}
	else{
		changeHeader("#cb0000ff","FM",this);
	}

	//ieterates through all selectors to get index of selected instruments 
	inputs = [];
	var enable_button=true;
	var ssg_count=0;
	var fm_count=0;
	$('.form-control').each(function(index){
		var option = $(this).children("option:selected").val();
		if(option=="Choose"){ //terminates for loop early if form not finished
			enable_button=false;
			return enable_button;
		}
		//incremenets appropriate indexes + appends to index
		if(option.indexOf("SSG")==0){ssg_count++;}
		else{fm_count++;}
		inputs.push(option);
	});
	if(inputs.length != parseInt(num_instruments)){//enable_button is left as false
		if($("#errors").css("display")!="none"){
			$("#errors").slideToggle();
		}
	} 
	else if(ssg_count>3 || fm_count > 6){ //too many FM or SSG insts added
		enable_button=false;
		var errortext = ssg_count>3 ? "SSG" : fm_count > 6 ? "FM":"";
		$("#errors").html("<div class = 'col-md-6'><div class='alert alert-danger mb-0 text-center' role='alert'> <i class='fas fa-exclamation-triangle'></i> <b>Error</b>: You have too many " + errortext + " instruments</div></div>");
		if($("#errors").css("display")=="none"){$("#errors").slideToggle();}
	}
	else{//enable button is left as true;
		if($("#errors").css("display")!="none"){
			$("#errors").slideToggle();
		}
	} 

	//changes button
	if(enable_button){$("#submit-button").attr("disabled",false);}
	else{$("#submit-button").attr("disabled",true);}
});

//sends form via stringified format to websocket
$("#instrument-form").on('submit',function(event){
	event.preventDefault();
	if($("#errors").css("display")!="none"){$("#errors").slideToggle()}
	$("#progress-group").slideToggle();
	socket.send(JSON.stringify(inputs)+"_"+pk);
});

