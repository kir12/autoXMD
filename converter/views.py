from django.shortcuts import render
from converter.forms import *
from django.http import JsonResponse
from django.conf import settings
from core.msdos import gen_mxl
from core.mmlmake import read_instrument_metadata
from core.instrument_dicts import get_fm_macro, FM_SCREENNAME, ALL_INSTRUMENTS
from .models import *
import os

ERRORS={
	"badfile":"You did not submit a MusicXML file",
	"long_filename":"Your filename exceeded 8 characters",
	"drums":"Your score includes drums which are <b>not</b> supported",
	"too_many_instruments":"Your score has more than 9 instruments",
	"common_filename":"Someone else has already submitted a file of the same name. Please re-name your file and try again."
}

#helper method: check if filename already exists inside a filefield
def common_filename(filename,typ):
	return filename in [os.path.basename(f).split(".")[0] for f in MusicXML.objects.all().values_list(typ,flat=True)]

#helper method: converts a explicit media path to a relative one
def relative_path(full_path):
	return os.path.relpath(full_path,settings.MEDIA_ROOT)

# VIEWS

def index(request):
	return render(request,"converter/converter_start.html",{'form':MusicXMLForm()})

def initial_read(request):
	if request.method=="POST":
		full_file=request.FILES['musicFile']
		filename=full_file.name.split(".")
		#case 1: fail to meet MS-DOS restriction
		if len(filename[0])>8:
			return JsonResponse({"output":ERRORS["long_filename"]},status=500)
		elif common_filename(filename[0],'musicFile') or common_filename(filename,'msczFile'): #ensures unique filename (so Django doesn't generate random filename/violate 8char limit)
			return JsonResponse({"output":ERRORS["common_filename"]},status=500)

		xmlPackage = MusicXML()
		if filename[1]=="mscz": #convert musescore to musicxml
			xmlPackage.msczFile = full_file
			xmlPackage.save()
			mxl_output = gen_mxl(xmlPackage.msczFile.path)
			if mxl_output[1]==True: #successfull musescore output
				xmlPackage.musicFile.name = relative_path(mxl_output[0])
				xmlPackage.save()
			else: #failed musescore output
				xmlPackage.delete()
				return JsonResponse({"output":mxl_output[0]}, status=500)
		elif filename[1] !="mxl": #didn't actually submit a mxl file
			return JsonResponse({"output":ERRORS["badfile"]}, status=500)
		else: #yes he did, ready to roll
			xmlPackage.musicFile = full_file
			xmlPackage.save()

		#actual processing begins here
		num_instruments,drum_index,unknown_inst,part_names = read_instrument_metadata(xmlPackage.musicFile.path)
		if len(drum_index) !=0 or num_instruments > 9: #return error if problem found
			xmlPackage.delete()
			return JsonResponse({"output":ERRORS["drums"]},status=500) if len(drum_index) !=0 else JsonResponse({"output":ERRORS["too_many_instruments"]},status=500)
		else: 
			for part_name in part_names:
				musicPart = InstrumentName(xmlFile=xmlPackage,name=part_name)
				musicPart.save()
			return JsonResponse({"pk":xmlPackage.pk,"num_instruments":num_instruments})

def instrument_fill(request):
	xmlPackage = MusicXML.objects.get(pk=int(request.GET.get('pk','')))
	table = {}
	for key,value in FM_SCREENNAME.items():
		table[key] = [get_fm_macro(ALL_INSTRUMENTS[key]),value]
	return render(request,"converter/instrument_fill.html",{'table':table,'part_names':xmlPackage.instrumentname.all()})
