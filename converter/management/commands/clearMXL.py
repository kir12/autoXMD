from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from converter.models import MusicXML
from datetime import datetime, timedelta

class Command(BaseCommand):
	help='Clears all old files that aren\`t being processed to keep disk size low'
	def add_arguments(self, parser): #use later to add arguments if desired
		pass
	def handle(self,*args, **options): #main logic here
		print(MusicXML.objects.all())
		MusicXML.objects.filter(timestamp__lt=timezone.now()-timedelta(hours=1)).delete()
		print(MusicXML.objects.all())
