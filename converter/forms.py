from django.forms import ModelForm
from converter.models import *

class MusicXMLForm(ModelForm):
	class Meta:
		model=MusicXML
		fields=['musicFile']
