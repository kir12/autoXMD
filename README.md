# autoXMD

A Django-powered converter between MusicXML and the MML language for composing music for the PC-98 created for the goal of facilitating making PC-98 arranges for the masses.
This readme will be technical-oriented so go to the website if you want a more general rundown.

## Dependencies:

If you wish to have a development instance of autoXMD running on your local machine, the below dependencies are required:

- A Linux Distro
- Python 3
- Musescore (or an equivalent sheet music software that can export to `.mxl`)
- Dosemu
- Simple DirectMedia Layer, (SDL 2) for usage with `pmdplay`

## Setup

0. Follow your Linux distro's instructions for installing any dependencies you haven't installed yet.
1. In `~/.bashrc`, add the environment variable `export DEV="true"`. This tells autoXMD the project to use development settings.
2. `git clone https://gitlab.com/kir12/autoXMD.git` and `cd /path/to/autoXMD/`
3. `python -m venv env && source env/bin/activate` to activate a virtual environment
4. `pip install -r requirements.txt` to install Python dependencies
5. `python manage.py migrate` to build database
6. `python manage.py runserver` to start up the development server. You will likely need to make your own development-only secret key.

Feel free to retrieve some scores from Musescore to use with testing autoXMD, or make your own ;). Just make sure to follow the score restrictions listed in the application.

## Technical Stack Rundown

If you're interested, here's a detailed explanation of autoXMD's stack:

1. When a user submits a file, (via an AJAX request) autoXMD checks it's a valid `.mxl` or a `.mscz` file, as well as other basic restrictions. If all requirements are met, autoXMD proceeds.
2. The user is prompted to select appropriate PC-98 instruments corresponding to each instrument channel in the original file. 
Whenever an instrument is changed, JQuery checks all instrument channels against maximum channel restrictions. Once all instrument channels have been filled (with no maximums reached) it enables Live Preview button.
3. Upon clicking, a WebSocket request is made and passed to Django Channels. Using `music21`, autoXMD begins ieterating through the source file and generating the `.mml` file.
4. Once the `.mml` file has finished generating, PMD is run via Dosemu and has its console output captured and examined. 
Any errors, if caught, end the process and are returned to the user.
5. If there are no errors, `pmdplay` is called to generate a `.wav` file from the `.m2` file.
6. All files that were generated in the process get returned to the user. Throughout this process, updates are sent back to the user via periodic WebSocket broadcasts.

## Issues & Requests

Odds are you'll find a bug in autoXMD or a fix for one of many features autoXMD currently lacks. 
If you do, please [submit an issue](https://gitlab.com/kir12/autoXMD/issues) or [a merge request](https://gitlab.com/kir12/autoXMD/merge_requests) at your leisure.
I appreciate it!
