#UTILITY DICTIONARIES AND METHODS FOR USAGE BY MMLMAKE AND IETERATION
#methods need by both mmlmake and iteration placed here to avoid cirtular imports

#SPECIFIC INSTRUMENTS PICKED FROM https://pastebin.com/YbPCdeR7 (HertzDevil's Instrument List)
#specific instruments from tutorial
#instrument selections from pedipanol
ALL_INSTRUMENTS={
	#FM INSTRUMENTS BEGIN HERE
	"backing_piano":"@012 4 7 21  4  2  4  2 40 0 12 3 0  24 12  4  6  2  0 0  4 7 0  21  4  2  4  2 35 0  6 7 0  24 12  4  6  2  0 0  2 3 0 ",
	#"guitar":"@016 0 6 28 17 18  7  4 32 0 10 6 0 31 14  6  7  2 38 1  0 3 0 31 14  6  7  2 28 1  0 6 0 31 16  4  7  1  0 1  1 3 0 ",
	"lead_power_backing_chords":"@023 4 7 31  0  0  2  0 30 3  4 3 0 31  2  0  7  3  0 0  4 3 0 31  0  0  2  0 31 3  2 7 0 31  2  0  7  3  0 0  4 7 0 ",
	"punchy_bass":"@029 2 7 31 18  4  5  8 30 0  8 0 0  31 12  4  5  2 50 0  1 3 0  31 12  4  5  2 20 0  0 3 0  31 16  4  6  1  0 0  1 4 0",
	"backing_chords":"@030 4 6 30  1  0  1  1 24 3  4 3 0  13  2  0  6  0  0 1  8 3 0  30  1  0  1  1 24 3  4 7 0  13  2  0  6  0  0 1  8 7 0",
	"accompany_arpeggio":"@043 5 7  31  0  0  0  0 29 0  2 0 0  31 14  3 15  3  1 0  8 0 0  31 14  3 15  3  1 0  4 0 0  31 14  3 15  3  1 0  4 0 0",
	"bells":"@063 4 6  27  4  0  6 15 35 1  3 3 0  31  7  0  7 15  0 2  1 4 0  31  7  0  4 14 41 1 14 7 0  31  8  0  7 15  0 0  4 7 0",
	"normal_bass":"@074 0 4  31  7  7  9  2 28 3  6 6 0  31  6  6  9  1 58 3  5 6 0  31  9  6  9  1 22 2  0 6 0  31  6  8  9 15  0 2  1 6 0 ",
	"piano":"@101 4 7 28  0  8  0  3 35 2 12 3 0  26 10  7  6  2  0 1  4 3 0 28  0  8  0  3 33 2 12 7 0  26 10  7  6  2  0 1  4 7 0 ",
	"accompany_arpeggio_2":"@181 4 7  31  0  0  0  0 27 0  8 7 0  31 16  0  8  2  0 0  4 7 0  31  0  0  0  0 30 0  8 3 0  31 16  0  8  2  0 0  4 3 0",
	"lead":"@250 5 7  31  3  4 15  0 29 0  4 1 0  21  1  4 15  0  0 0  8 1 0 31  6  4 15  0  0 0  2 1 0  21  1  4 15  0  0 0  4 0 0 ",
	#SSG INSTRUMENTS BEGIN HERE
	"ssg_default":"@0 ",
	"ssg_synth_1":"@1 ",
	"ssg_synth_2":"@2 ",
	"ssg_synth_3":"@3 ",
	"ssg_piano_1":"@4 ",
	"ssg_piano_2":"@5 ",
	"ssg_mariamba":"@6 ",
	"ssg_strings":"@7 ",
	"ssg_brass_1":"@8 ",
	"ssg_brass_2":"@9 ",
}

ALL_NUMBERS={
	#FM INSTRUMENTS BEGIN HERE
	12:'backing_piano',
	23:'lead_power_backing_chords',
	29:'punchy_bass',
	30:'backing_chords',
	43:'accompany_arpeggio',
	63:'bells',
	74:'normal_bass',
	101:'piano',
	181:'accompany_arpeggio_2',
	250:'lead',
	#SSG INSTRUMENTS BEGIN HERE 
	0:"ssg_default",
	1:"ssg_synth_1",
	2:"ssg_synth_2",
	3:"ssg_synth_3",
	4:"ssg_piano_1",
	5:"ssg_piano_2",
	6:"ssg_mariamba",
	7:"ssg_strings",
	8:"ssg_brass_1",
	9:"ssg_brass_2",
}

ALL_TRANSPOSE={
	#FM_INSTRUMENTS BEGIN HERE
	"backing_piano":0,
	"lead_power_backing_chords":0,
	"punchy_bass":2,
	"backing_chords":-1,
	"accompany_arpeggio":0,
	"bells":1,
	"normal_bass":2,
	"piano":-1,
	"accompany_arpeggio_2":-1,
	"lead":0,
	#SSG BEGINS HERE
	"ssg_default":0,
	"ssg_synth_1":0,
	"ssg_synth_2":0,
	"ssg_synth_3":0,
	"ssg_piano_1":0,
	"ssg_piano_2":0,
	"ssg_mariamba":0,
	"ssg_strings":0,
	"ssg_brass_1":0,
	"ssg_brass_2":0,
	#SPECIAL DRUMS CASE
	"drums":0
}
FM_SCREENNAME={
	#FM BEGINS HERE
	"backing_piano":"Backing Piano",
	"lead_power_backing_chords":"Lead, Backing Power Chords, Violin",
	"punchy_bass":"Punchy Bass",
	"backing_chords":"Backing Chords",
	"accompany_arpeggio":"Accompanying Arpeggios",
	"bells":"Bells",
	"normal_bass":"Normal Bass",
	"piano":"Lead Piano",
	"accompany_arpeggio_2":"Accompanying Arpeggios with SSG",
	"lead":"Lead and Rarely Backing Power Chords",
	#SSG BEGINS HERE
	"ssg_default":"SSG Default",
	"ssg_synth_1":"SSG Synth Type 1",
	"ssg_synth_2":"SSG Synth Type 2",
	"ssg_synth_3":"SSG Synth Type 3",
	"ssg_piano_1":"SSG Piano Type 1",
	"ssg_piano_2":"SSG Piano Type 2",
	"ssg_mariamba":"SSG Glockenspiel/Mariamba",
	"ssg_strings":"SSG Strings",
	"ssg_brass_1":"SSG Brass Type 1",
	"ssg_brass_2":"SSG Brass Type 2",
}

NOTE_DURATIONS={
	"whole":1,
	"half":2,
	"quarter":4,
	"eighth":8,
	"16th":16,
	"32nd":32,
}

DRUM_NOTES={
}

#converts index of instrument to FM channel
def get_instrument_char(index):
	return chr(65+index)

#returns simple class name of object for usage in music21 ieteration
def get_class_name(obj):
	return obj.__class__.__name__

#returns shorthand macro of instruments starting with "@ " 
def get_fm_macro(entry):
	return entry[:entry.find(" ")]


