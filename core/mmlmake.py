#main mml method and all non-song methods
#iteration is complex so it gets its own file

#NOTE: divide tempo by 2 from musicxml for pmd
#NOTE: PMD requires max 8 letters for filename
#NOTE: autoXMD takes the HIGHEST note of a chord and ONLY the first voice (to be configured maybe)
#NOTE: first note un owen was her -> octave 5. use that to implement transpostions for FM instruments

import os
from .ieteration import *
from music21 import *
from math import floor
from fractions import Fraction
from .instrument_dicts import *

#attempts to find appropriate metadata in song, and substitues with placeholders when necessary
def get_metadata(f, score):

	entries = {"#Title":score.metadata.title,"#Composer":score.metadata.composer,"#Arranger":",".join(contrib.name for contrib in score.metadata.contributors)}
	for key, value in entries.items():
		#todo: export values for return back to web function
		if(value!=None and value!=""):
			f.write(key+" "+value+"\r\n")
		else:
			f.write(key+" N/A\r\n")

	#print(score.metadata.tempo) todo: how is tempo set if it changes in song?
	f.write("#Option /v/c\r\n")
	f.write("#Filename .M2\r\n")


#gets instruments and pulls up appropriate fm instruments
def instruments(f,score,all_instruments):
	f.write("\r\n;INSTRUMENTS\r\n\r\n")

	#generates lists of both entire score parts and instrument names specifically 
	score_parts = [part for part in score if get_class_name(part) == "Part" or get_class_name(part)=="PartStaff"]

	for instrument in all_instruments:
		#TODO: THIS IS A TEMPORARY FIX, REPLACE WITH INST METADATA FEATURE IN DRUMS BRANCH
		if "ssg" not in instrument and "drums" not in instrument:
			f.write(ALL_INSTRUMENTS[instrument]+"\r\n")
	return score_parts

#combines drum instruments into a single part and replaces drums with singular drum part
def combine_drums(score_parts,drum_index):
	drum_score = stream.Score()
	for i in drum_index:
		drum_score.insert(0,score_parts[i])
	drum_score = drum_score.chordify()
	environment.set('musicxmlPath','/usr/bin/musescore')
	drum_score.show()
	print(type(drum_score))
	for thing in drum_score:
		print(type(thing))
	#print([thing for thing in drum_score if get_class_name(thing)=="Part"])

#returns number of instruments for populating instruments later
def read_instrument_metadata(TEST_FILE):
	score = converter.parse(TEST_FILE)
	parts = [part for part in score if get_class_name(part) == "Part" or get_class_name(part)=="PartStaff"]
	drum_index = []
	unknown_inst=[]
	part_names=[]
	for i in range(0,len(parts)):
		part_names.append(parts[i].partName)
		try:
			clefname = next(thing for thing in parts[i] if get_class_name(thing)=="Measure").clef.__class__.__name__
			#base_class =  [obj.__name__ for obj in instrument.fromString(parts[i].id).__class__.__bases__]
			#if next((inst for inst in base_class if "Percussion" in inst),None) != None: drum_index.append(i)
			if clefname=="PercussionClef":drum_index.append(i)
		except Exception as e:
			if parts[i].id in ["Hand Clap"]: drum_index.append(i) #manual percussion verification 
			else:unknown_inst.append(i) #all other unknown instruments move out
	return len(parts), drum_index, unknown_inst,part_names

#given a list of instruments, determines their macro order
#note: instrument validation (including drum combination) will happen before this method
def determine_order(all_instruments):
	order = []
	ssg_count = 0
	fm_count=0
	for instrument in all_instruments:
		if 'ssg' in instrument:
			order.append(chr(71+ssg_count))
			ssg_count+=1
		elif 'drums' in instrument: order.append('K')
		else:
			order.append(get_instrument_char(fm_count))
			fm_count+=1
	return order

#the main file that controls all the MML production
def mmlmake(TEST_FILE,all_instruments,drum_index,broadcast_params):
		
	#score = converter.parse(os.path.dirname(os.path.realpath(__file__)) + '/xml/god.mxl')
	score = converter.parse(TEST_FILE)
	broadcast_params[0](broadcast_params[1],30,"Processing Score in Music21")

	basefilename = os.path.splitext(os.path.basename(TEST_FILE))[0]

	filepath=os.path.dirname(os.path.dirname(TEST_FILE))+"/PMD/songs/" + basefilename + ".mml"
	f = open(filepath,"w")
	get_metadata(f, score)
	score_parts = instruments(f,score,all_instruments)

	f.write("\r\n;SONG\r\n\r\n")

	if len(drum_index) > 1: combine_drums(score_parts,drum_index)

	inst_order = determine_order(all_instruments)
	print(inst_order)

	ieteration(f, score_parts,all_instruments,inst_order,0,[])

	f.close()

	return filepath,basefilename

#given a list of screen-name PC-98 instruments (provided from browser), converts them to internal name format
def gen_all_instruments(screen_select):
	invd = {v:k for k,v in FM_SCREENNAME.items()}
	return [invd[i] for i in screen_select]
