#ACTUAL SONG PROCESSING GOES HERE

from music21 import *
import os
from math import floor
from fractions import Fraction
from .instrument_dicts import *

#given a part, writes instrument channel + ID and calculates transposition/octave count. (pre-measure processing)
#INPUT: score_parts (raw parts list), instrument order, instrument list, instrument index
#OUTPUT: measure-only list, length of measures (for access outside for loop), transposition if exists, and octave of first note
def pre_process(f, score_parts,inst_order,all_instruments,i):

	#prints FM channel letter and corresponding part number to mml
	print(score_parts[i])
	f.write(inst_order[i]+" ")
	#ignores drums since no instrument ID exists for rhythm channel
	if all_instruments[i] != "drums": f.write(get_fm_macro(ALL_INSTRUMENTS[all_instruments[i]])+" ")

	#generates list of only measures inside part, and prints send opening octave to mml
	measures = [measure for measure in score_parts[i] if get_class_name(measure) == "Measure"]
	len_measures = len(measures)

	transposition=score_parts[i].getInstrument().transposition
	#generates octave_count, the opening octave for a group of 10 measures. two cases for note + chord
	#note: initially processed as string since 0 is interpreted as False
	if transposition is not None:
		octave_count = next((str(note.transpose(transposition).octave+ALL_TRANSPOSE[all_instruments[i]]) for measure in measures for note in measure if get_class_name(note)=="Note"),None) \
			or next((str(chord[len(chord)-1].transpose(transposition).octave+ALL_TRANSPOSE[all_instruments[i]]) for measure in measures for chord in measure if get_class_name(chord)=="Chord"),None)
	else:
		octave_count = next((str(note.octave+ALL_TRANSPOSE[all_instruments[i]]) for measure in measures for note in measure if get_class_name(note)=="Note"),None) \
			or next((str(chord[len(chord)-1].octave+ALL_TRANSPOSE[all_instruments[i]]) for measure in measures for chord in measure if get_class_name(chord)=="Chord"),None)
	if octave_count is not None:
		octave_count = int(octave_count)
		f.write("o" + str(octave_count)+" ")
	
	return measures, len_measures, transposition, octave_count

#checks if a measure has multiple voices
#case 1) not drum: makes only one voice
#case 2) drum: chordifys measure
def check_voices(measure,inst_id):
	#special case forif measure is separated into voices --> takes voice 1
	if inst_id == "drums": 
		test= measure.chordify()
		test.show('text')
		return test
	elif measure.hasVoices():
		new_measure = stream.Measure()
		voice_appended=False
		for measure_part in measure:
			if get_class_name(measure_part)!="Voice": new_measure.append(measure_part)
			elif voice_appended==False:
				for note_thing in measure_part: new_measure.append(note_thing)
				voice_appended=True
		return new_measure
	else: return measure

#generic function that either writes to file or writes to drum
def song_write(f,expression,drum_measure,inst):
	if inst!="drums": f.write(expression)
	else: drum_measure+=expression
	return drum_measure

#main method for processing notes, controls submethods for each note portion 
def note_process(f,measure_part,octave_count,transposition,inst,graceVals,drum_measure):

	note_record='' #will capture most recently written note/rest in case complex note encountered

	if get_class_name(measure_part)=="Rest":
		note_record="r"
		f.write(note_record)
	elif inst == "drums":
		temp_measure_part = chord.Chord([measure_part]) if get_class_name(measure_part)=="Note" else measure_part
		for note in temp_measure_part:
			print(note)
	else:
		if get_class_name(measure_part)=="Chord": #strip away all the lower notes
			measure_part = measure_part[len(measure_part)-1]	

		if octave_count is not None and get_class_name(measure_part)=="Note":
			#executes transposition if there is one
			if transposition is not None: measure_part = measure_part.transpose(transposition) 
			#calculates difference in octaves (if any) and outputs to mml
			octave_diff = measure_part.octave+ALL_TRANSPOSE[inst] - octave_count
			octave_diff_char = ">"*abs(octave_diff) +" " if octave_diff>0 else "<"*abs(octave_diff) +" " if octave_diff<0 else ""
			octave_count = measure_part.octave+ALL_TRANSPOSE[inst]
			#if grace note detected, updates graceVals for usage in next loop
			grace_note_property=""
			if measure_part.duration.type=="zero":
				graceVals[0]+=1
				graceVals[1]=True
				graceVals[2]="" if graceVals[3] is None else "+" if graceVals[3]>measure_part else "-"
				graceVals[3]=measure_part
			#once first non-grace note detected, enters proper grace note property
			elif graceVals[1]:
				graceVals[2]="+" if graceVals[3]>measure_part else "-"
				grace_note_property = "S2," + graceVals[2]+str(graceVals[0])+",0 "
				graceVals=[0,False,"",None,True]
			#writes note
			if measure_part.duration.type!="zero":
				note_record=measure_part.name.lower().replace("#","+")
				f.write(octave_diff_char + grace_note_property + note_record)
	
	#escapes out if note is grace note
	if measure_part.duration.type!="zero":
		if not measure_part.duration.isComplex:
			duration = NOTE_DURATIONS[measure_part.duration.components[0][0]]
			#check if triplet
			if "Triplet" in measure_part.duration.fullName or "Sextuplet" in measure_part.duration.fullName:
				#f.write(note_record+str(int(duration*1.5)))
				f.write(str(int(duration*1.5)))
			else:
				#f.write(note_record+str(duration))
				f.write(str(duration))

			f.write("."*measure_part.duration.dots)
		
		else:
			for i in range(0,len(measure_part.duration.components)):
				duration=NOTE_DURATIONS[measure_part.duration.components[i][0]]
				if i==0:f.write(str(duration))
				else:f.write(note_record+str(duration))
				f.write("."*measure_part.duration.components[i].dots)
				if i < len(measure_part.duration.components)-1:f.write(" & ")
		if measure_part.tie is not None and measure_part.tie.type != "stop": f.write(" & ") #todo: address drum problem

		#if i < len(measure_part.duration.components)-1:f.write(" & ")

	#resets grace settings to zero
	if graceVals[4]:
		f.write(" S0 ")
		graceVals[4]=False
	elif measure_part.duration.type!="zero":
		f.write(" ")
	return octave_count


#ietereates through measures up to a certain section (e.g. a repeat, new letter, etc.) and enters as block
#recursively calls itself to start new section
#drum_parts encloses the drum measures
def ieteration(f, score_parts,all_instruments, inst_order, measure_start_index, drum_parts):

	#for thing in score_parts[0]: todo: figure out how to handle special cases
		#if get_class_name(thing) == "Glissando":
		#print(thing)
	f.write(";STARTING MEASURE " + str(measure_start_index+1) + "\r\n\r\n")
	#sets variables for new starting index and length of measures
	measure_new_start_index=-1
	len_measures=-1
	measure_counter=-1
	tempo_tracker=[0,-1] #the tempo amount, and measure_counter

	for i in range(0,len(score_parts)):

		measures, len_measures, transposition, octave_count = pre_process(f, score_parts,inst_order,all_instruments,i)

		#ieterates through measures starting from start_index
		#continues until either loop is terminated or end of measures reached
		measure_counter = measure_start_index
		continue_iet=True
		#for measure in [measures[0],measures[1],measures[2],measures[3]]:
		while continue_iet and measure_counter < len_measures:

			#checks if measure has multiple voices and acts accordingly
			measures[measure_counter] = check_voices(measures[measure_counter],all_instruments[i])

			#how many notes, if previous note was grace note, direction, previous grace note, need to reset grace
			graceVals=[0,False,"",None,False]
			
			#temporary enclosure for drum measure
			drum_measure = ""

			for measure_part in measures[measure_counter]:

				#case -1: metronome
				if get_class_name(measure_part)=="MetronomeMark":
					tempo_tracker=[measure_part.getQuarterBPM(),measure_counter]
					final_tempo = floor(tempo_tracker[0]/2) if floor(tempo_tracker[0]/2) >= 18 else 18 #if final calculated tempo < 18
					drum_measure=song_write(f,"t"+str(final_tempo)+" ",drum_measure,all_instruments[i])
				#case 0: time signature
				elif get_class_name(measure_part)=="TimeSignature":
					drum_measure=song_write(f,"Z"+str(floor(96*float(Fraction(measure_part.ratioString))))+" ",drum_measure,all_instruments[i])
				#case 1: opening repeat sign
				elif get_class_name(measure_part)=="Repeat" and measure_part.direction == "start":
					drum_measure=song_write(f,"[",drum_measure,all_instruments[i])
				#case 1.25:dynamics
				elif get_class_name(measure_part)=="Dynamic":
					drum_measure=song_write(f,"v"+str(floor(measure_part.volumeScalar*16))+" ",drum_measure,all_instruments[i])
				#case 2: standard note
				elif get_class_name(measure_part)=="Note" or get_class_name(measure_part) == "Rest" or get_class_name(measure_part)=="Chord":
					octave_count = note_process(f,measure_part,octave_count,transposition,all_instruments[i],graceVals,drum_measure)
				#case 3: ending repeat
				elif get_class_name(measure_part)=="Repeat" and measure_part.direction=="end":
					f.write("]2 ")

				#ends line after 10 measures
				if measure_counter % 9 == 0 and measure_counter != 0:
					measure_start_new_index = measure_counter+1
					continue_iet=False
			measure_counter+=1

		f.write("\r\n")
	f.write("\r\n")	
	#print("measure_start_new_index: " + str(measure_start_new_index))
	if measure_counter < len_measures:
		return ieteration(f,score_parts,all_instruments,inst_order, measure_start_new_index, drum_parts)
	else:
		return 0


