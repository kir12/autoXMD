import sys
import os
import subprocess
import signal
import datetime
import shutil
from dateutil import parser
from .mmlmake import mmlmake, read_instrument_metadata
from .instrument_dicts import ALL_NUMBERS
from math import floor

#given string stamp, converts into seconds
def get_seconds(stamp):
	t=parser.parse("00:"+stamp) #todo: get this into seconds
	return datetime.timedelta(minutes=t.minute,seconds=t.second).seconds

#assumption: filepath starts with PMD/songs/
def compileM(mml_file,basefilename,autoexit=True):

	#generate .M file directory and removes it if exists already
	mfile_dir=mml_file.replace("mml","m2")
	print(mfile_dir)
	if os.path.isfile(mfile_dir):
		os.remove(mfile_dir)

	print(autoexit)
	#generates directory to cd into PMD directory and specific PMD command
	cd_dir=os.path.dirname(os.path.realpath(__file__)).replace(os.path.expanduser("~")+"/","").replace("/","\\").upper()+"\\PMD"
	pmd_command="MC\\MCE.EXE /v SONGS\\"+basefilename.upper()+".MML\rexitemu\r"
	#generates command used with subprocess.run
	chop=["dosemu","-dumb","-input",repr("D:\rcd "+cd_dir+"\r"+pmd_command).strip("'")]
	result = subprocess.run(chop,stdout=subprocess.PIPE)
	#extracts and parses result
	output=result.stdout.decode('utf-8')
	start = output.find(".MML file -->",0,len(output))
	end = output.find("\nD:",start,len(output))
	print(output[start:end])

	return [os.path.isfile(mfile_dir),mfile_dir,output[start:end]]

#converts raw mcsz to mxl compatible with music21
def gen_mxl(filename):
	#os.system("musescore \"" + filename "\" -o \"" + os.path.splitext(filename)+".mxl""\"")	
	output_filename=os.path.splitext(filename)[0]+".mxl"
	musescore_type="musescore" if shutil.which("musescore") != None else "xvfb-run musescore3"
	command = musescore_type+ " " + filename + " -o " + output_filename
	result = subprocess.run(command.split(),stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
	if result.stderr != None or result.returncode != 0: 
		return [result.stdout.decode('utf-8'),False]
	else:
		return [output_filename,True]

#extracts wav file from m2 file, courtesy of gzaffin
def gen_flac(filename):
	#puts together glibc version
	output = os.path.splitext(filename)[0]+".wav"
	glibc_version = subprocess.run(["ldd","--version"], stdout=subprocess.PIPE).stdout.decode('utf-8').split("\n")[0].split(" ")
	glibc_version = glibc_version[len(glibc_version)-1]

	#runs pmdplay
	pmdplay = subprocess.Popen((os.path.abspath(os.path.dirname(__file__)) + "/PMD/pmdplay_" + glibc_version + " " + filename + " " + output).split(" "), stdout = subprocess.PIPE,universal_newlines=True).wait()

	#export as flac and remove old wav
	output_flac=os.path.splitext(filename)[0]+".flac"
	subprocess.run(["sox",output,output_flac])
	os.remove(output)

	return output_flac

#this code will (eventually) be broken up into Django space
if __name__== "__main__":

	#THIS PORTION RUNS BEFORE USER SUBMITS INSTRUMENTS (VIEW #1)
	#process runs via ajax and prompts redirect once completed

	#converts raw mcsz files to mxl files
	filename_raw = sys.argv[1]
	if os.path.splitext(filename_raw)[1]==".mscz":
		mxl_output = gen_mxl(filename_raw)
		if mxl_output[1]==True: filename_raw = mxl_output[0]
		else: print("INVALID MCSZ") #will be handled differently in django
	
	#get instrument metadata for django presentation
	num_instruments,drum_index,unknown_inst = read_instrument_metadata(filename_raw)
	print("NUMBER OF INST "+str(num_instruments))
	print("DRUM CHANNELS: "+str(drum_index))
	print("UNKOWN CHANNELS: "+str(unknown_inst))

	#THIS PORTION RUNS AFTER INTS SELECTED
	#same process as above

	#user interface will guide user to proper instrument usage
	all_instruments=[]
	fm_no=[]
	for i in range(0,num_instruments):
		#inst = input("Enter the instrument number. (" + str(i+1) +") ")
		#inst=30
		#all_instruments.append(ALL_NUMBERS[int(inst)])
		if fm_no[i] != "drums": all_instruments.append(ALL_NUMBERS[fm_no[i]])
		else: 
			print(fm_no[i])
			all_instruments.append(fm_no[i])

	print(all_instruments)

	mml_file, basefilename = mmlmake(filename_raw,all_instruments,drum_index)
	if len(sys.argv)==3:
		print(compileM(mml_file,basefilename,autoexit=bool(int(sys.argv[2]))))
	else:
		print(compileM(mml_file,basefilename))
